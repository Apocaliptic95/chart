﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMCG.Database
{
    interface IDataBase
    {
        IEnumerable<PlotData> getPlotDataList();
        void savePlotData(PlotData plotData);  
    }
}
