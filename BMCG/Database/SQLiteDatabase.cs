﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMCG.Database
{
    class SQLiteDatabase : IDataBase
    {

        public SQLiteDatabase()
        {
            if(!File.Exists("Database/ExpressionDatabase.db"))
            {
                SQLiteConnection.CreateFile("Database/ExpressionDatabase.db");
            }
        }

        public IEnumerable<PlotData> getPlotDataList()
        {
            using (var context = new ExpressionDatabaseEntities())
            {
                IQueryable<Expression> rtn = from temp in context.Expressions select temp;
                List<Expression> list = rtn.ToList();
                List<PlotData> plots = new List<PlotData>();
                foreach (Expression ex in list)
                    plots.Add(PlotData.Deserialize(ex.PlotData));
                return plots;
            }
        }

        public void savePlotData(PlotData plotData)
        {
            using (var context = new ExpressionDatabaseEntities())
            {
                Expression ex = new Expression();
                ex.PlotData = PlotData.Serialize(plotData);
                context.Expressions.Add(ex);
                context.SaveChanges();
            }
        }

    }
}
