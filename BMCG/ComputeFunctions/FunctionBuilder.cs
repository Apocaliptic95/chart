﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMCG.Model
{
    public static class FunctionBuilder
    {
        public static Func<double,double> createPolynomialFunction(List<Item> itemList)
        {
            return (x) =>
            {
                double result = 0;
                foreach(Item item in itemList)
                {
                    result += item.multiplier * Math.Pow(x, item.power);
                }
                return result;
            };
        }
    }
}
