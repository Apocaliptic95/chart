﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMCG.Model
{
    public static class PolynominalDerivative
    {
        public static List<Item> Derivative(List<Item> itemList)
        {
            List<Item> result = new List<Item>();
            foreach(Item item in itemList)
            {
                result.Add((Item)item.Clone());
            }
            foreach(Item item in result)
            {
                item.multiplier *= item.power;
                item.power -= 1;
            }
            result = result.Where(x => x.multiplier != 0).ToList();
            return result;
        }
    }
}
