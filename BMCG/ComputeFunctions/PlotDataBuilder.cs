﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMCG.Model
{
    public class PlotDataBuilder
    {
        public static PlotData MakePlotData(string expression,double from, double to)
        {
            List<Item> exp = ExpressionStringDecoder.resolveExpression(expression);
            List<Item> der = PolynominalDerivative.Derivative(exp);
            Func<double, double> function = FunctionBuilder.createPolynomialFunction(der);
            double step = (to - from) / 100;
            var x = Range(from, to, step);
            var y = x.Select(t => function(t));
            return new PlotData(x,y,"("+expression+")'",from,to);
        }

        private static IEnumerable<double> Range(double from, double to, double step)
        {
            if (step <= 0.0) step = (step == 0.0) ? 1.0 : -step;

            if (from <= to)
            {
                for (double d = from; d <= to; d += step) yield return d;
            }
            else
            {
                for (double d = from; d >= to; d -= step) yield return d;
            }
        }
    }
}
