﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BMCG.Model
{
    public static class ExpressionStringDecoder
    {
        public static string _expression { get; private set; }

        public static List<Item> resolveExpression(string expression)
        {
            _expression = expression;
            return getItemList();
        }

        private static List<Item> getItemList()
        {
            List<Item> itemList = new List<Item>();
            int ac_pos = 0;
            int ac_len = 0;
            Item ac_item = new Item();
            for(int index=0; index < _expression.Length; index++)
            {
                if (_expression[index] == ' ')
                    continue;
                else if (_expression[index] == '+')
                    continue;
                else if (Char.IsDigit(_expression[index]) || _expression[index] == '(')
                {
                    ac_pos = index;
                    ac_len = 0;
                    while (index < _expression.Length  
                        && _expression[index] != 'x' 
                        && _expression[index] != ' ' 
                        && _expression[index] != '+')
                    {
                        ac_len++;
                        index++;
                    }
                    ac_item.multiplier = parseExpression(_expression.Substring(ac_pos, ac_len));
                    if (index < _expression.Length 
                        && _expression[index] == 'x')
                    {
                        index++;//skip x
                        if (index < _expression.Length 
                            && _expression[index] == '^')
                        {
                            index++; //skip ^

                            ac_pos = index;
                            ac_len = 0;
                            while (index < _expression.Length 
                                && _expression[index] != ' ' 
                                && _expression[index] != '+')
                            {
                                ac_len++;
                                index++;
                            }
                            ac_item.power = parseExpression(_expression.Substring(ac_pos, ac_len));
                        }
                        else
                            ac_item.power = 1;
                    }
                    itemList.Add(ac_item);
                    ac_item = new Item();
                }
                else if(_expression[index] == 'x')
                {
                    int i = index;
                    ac_item.multiplier = 1;
                    index++;//skip x
                    if (index < _expression.Length 
                        && _expression[index] == '^')
                    {
                        index++; //skip ^
                        ac_pos = index;
                        ac_len = 0;
                        while (index < _expression.Length 
                            && _expression[index] != ' ' 
                            && _expression[index] != '+')
                        {
                            ac_len++;
                            index++;
                        }
                        ac_item.power = parseExpression(_expression.Substring(ac_pos, ac_len));
                    }
                    else
                        ac_item.power = 1;
                    itemList.Add(ac_item);
                    ac_item = new Item();
                }
            }
            return itemList;
        }

        private static double parseExpression(string expression)
        {
            if (expression[0] == '(' && expression[expression.Length - 1] == ')')
            {
                return parseExpression(expression.Substring(1, expression.Length - 2));
            }
            else if (Regex.IsMatch(expression, @"^[-]?[1-9][0-9]*[,]?[0-9]*$"))
            {
                return Double.Parse(expression);
            }
            else if (Regex.IsMatch(expression, @"^[-]?[1-9][0-9]*[,]?[0-9]*[/][-]?[1-9][0-9]*[,]?[0-9]*$"))
            {
                int divIndex = expression.IndexOf('/');
                return parseExpression(expression.Substring(0, divIndex)) / parseExpression(expression.Substring(divIndex + 1, expression.Length - 1 - divIndex));
            }
            else return 0;
        }
    }

    public class Item : ICloneable
    {
        public double multiplier { get; set; }
        public double power { get; set; }

        public object Clone()
        {
            return new Item { multiplier = this.multiplier, power = this.power };
        }
    }
}
