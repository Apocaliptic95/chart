﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using BMCG.Model;
using BMCG.Database;
using System.Text.RegularExpressions;
using System.Windows.Media;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Threading;

namespace BMCG
{
    public class MainWindowViewModel : PropertyChangedBase
    {
        IDataBase db;

        public MainWindowViewModel()
        {
            _expressionEditorBorder = "#FF000000";
            _fromBorder = "#FF000000";
            _toBorder = "#FF000000";
            db = new SQLiteDatabase();
            Expressions = new BindableCollection<PlotData>();
            FillExpressions();
        }

        private string _expressionEditor;
        public string ExpressionEditor
        {
            get
            {
                return _expressionEditor;
            }
            set
            {
                _expressionEditor = value;
                NotifyOfPropertyChange("ExpressionEditor");
            }
        }

        private string _from;
        public string From
        {
            get
            {
                return _from;
            }
            set
            {
                _from = value;
                NotifyOfPropertyChange("From");
            }
        }

        private string _to;
        public string To
        {
            get
            {
                return _to;
            }
            set
            {
                _to = value;
                NotifyOfPropertyChange("To");
            }
        }

        private string _expressionEditorBorder;
        public string ExpressionEditorBorder
        {
            get
            {
                return _expressionEditorBorder;
            }
            set
            {
                _expressionEditorBorder = value;
                NotifyOfPropertyChange("ExpressionEditorBorder");
            }
        }

        private string _fromBorder;
        public string FromBorder
        {
            get
            {
                return _fromBorder;
            }
            set
            {
                _fromBorder = value;
                NotifyOfPropertyChange("FromBorder");
            }
        }

        private string _toBorder;
        public string ToBorder
        {
            get
            {
                return _toBorder;
            }
            set
            {
                _toBorder = value;
                NotifyOfPropertyChange("ToBorder");
            }
        }

        private BindableCollection<PlotData> _expressions;
        public BindableCollection<PlotData> Expressions
        {
            get
            {
                return _expressions;
            }
            set
            {
                _expressions = value;
                NotifyOfPropertyChange(() => Expressions);
            }
        }

        private PlotData _expression;
        public PlotData SelectedExpression
        {
            get
            {
                return _expression;
            }
            set
            {
                if (_expression != value)
                {
                    _expression = value;
                    NotifyOfPropertyChange(() => SelectedExpression);
                }
            }
        }    

        public void Draw()
        {
            FromBorder = "#FF000000";
            ToBorder = "#FF000000";
            ExpressionEditorBorder = "#FF000000";
            bool fr = IsCorrectRange(From);
            bool to = IsCorrectRange(To);
            bool ex = IsCorrectExpression(ExpressionEditor);
            bool gt = true;
            if(fr && to)
                gt = (double.Parse(From) < double.Parse(To)) ? true : false;
            if(!fr)
            {
                FromBorder = "#FFFF0000";
            }
            if(!to || !gt)
            {
                ToBorder = "#FFFF0000";
            }
            if(!ex)
            {
                ExpressionEditorBorder = "#FFFF0000";
            }
            if(fr && to && ex && gt)
            {
                FromBorder = "#FF00FF00";
                ToBorder = "#FF00FF00";
                ExpressionEditorBorder = "#FF00FF00";
                BackgroundWorker bw = new BackgroundWorker();
                bw.DoWork += Bw_DoWork;
                bw.WorkerReportsProgress = true;
                bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
                bw.RunWorkerAsync();
            }
        }

        public void Clear()
        {
            ExpressionEditor = "";
            From = "";
            To = "";
            string defcol = "#FF000000";
            ExpressionEditorBorder = defcol;
            FromBorder = defcol;
            ToBorder = defcol;
        }

        public bool CanClear()
        {
            return true;
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            FillExpressions();
            if(Expressions.Count > 0)
                SelectedExpression = Expressions.Last();
            FromBorder = "#FF000000";
            ToBorder = "#FF000000";
            ExpressionEditorBorder = "#FF000000";
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            PlotData pd = ProcessExpression();
            if(pd.x != null && pd.y != null)
                db.savePlotData(pd);
        }

        private void FillExpressions()
        {
            Expressions.Clear();
            foreach (PlotData pda in db.getPlotDataList())
            {
                Expressions.Add(pda);
            }
        }

        private PlotData ProcessExpression()
        {
            return PlotDataBuilder.MakePlotData(ExpressionEditor, double.Parse(From), double.Parse(To));
        }

        public bool CanDraw()
        {
            return true;
        }

        private bool IsCorrectExpression(string expression)
        {
            if(Regex.IsMatch(expression, @"^((((([1-9][0-9]*([,][0-9]*)?)|([(][-]?[1-9][0-9]*([,][0-9]*)?([/][-]?[1-9][0-9]*([,][0-9]*)?)?[)]))?[x][\^](([1-9][0-9]*([,][0-9]*)?)|([(][-]?[1-9][0-9]*([,][0-9]*)?([/][-]?[1-9][0-9]*([,][0-9]*)?)?[)])))|((([1-9][0-9]*([,][0-9]*)?)|([(][-]?[1-9][0-9]*([,][0-9]*)?([/][-]?[1-9][0-9]*([,][0-9]*)?)?[)]))?[x])|(([1-9][0-9]*([,][0-9]*)?)|([(][-]?[1-9][0-9]*([,][0-9]*)?([/][-]?[1-9][0-9]*([,][0-9]*)?)?[)])))(([ ]?[+][ ]?)(((([1-9][0-9]*([,][0-9]*)?)|([(][-]?[1-9][0-9]*([,][0-9]*)?([/][-]?[1-9][0-9]*([,][0-9]*)?)?[)]))?[x][\^](([1-9][0-9]*([,][0-9]*)?)|([(][-]?[1-9][0-9]*([,][0-9]*)?([/][-]?[1-9][0-9]*([,][0-9]*)?)?[)])))|((([1-9][0-9]*([,][0-9]*)?)|([(][-]?[1-9][0-9]*([,][0-9]*)?([/][-]?[1-9][0-9]*([,][0-9]*)?)?[)]))?[x])|(([1-9][0-9]*([,][0-9]*)?)|([(][-]?[1-9][0-9]*([,][0-9]*)?([/][-]?[1-9][0-9]*([,][0-9]*)?)?[)]))))*)$"))
                return true;
            else
                return false;
        }

        private bool IsCorrectRange(string range)
        {
            double d;
            if (Double.TryParse(range, out d))
                return true;
            else
                return false;
        }
    }
}
