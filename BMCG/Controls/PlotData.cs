﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace BMCG
{
    [Serializable]
    public class PlotData
    {
        private string _expression;
        public string Expression
        {
            get
            {
                return _expression;
            }
            set
            {
                _expression = value;
            }
        }

        private double _from;
        public double From
        {
            get
            {
                return _from;
            }
            set
            {
                _from = value;
            }
        }

        private double _to;
        public double To
        {
            get
            {
                return _to;
            }
            set
            {
                _to = value;
            }
        }

        public string Name
        {
            get
            {
                return Expression + "   xε(" + From.ToString() + "," + To.ToString() + ")";
            }
        }

        private List<double> _x;
        public IEnumerable<double> x
        {
            get
            {
                return _x;
            }
            set
            {
                _x = value.ToList<double>();
            }
        }

        private List<double> _y;
        public IEnumerable<double> y
        {
            get
            {
                return _y;
            }
            set
            {
                _y = value.ToList<double>();
            }
        }

        public PlotData(IEnumerable<double> x, IEnumerable<double> y,string expression, double from, double to)
        {
            _x = x.ToList<double>();
            _y = y.ToList<double>();
            _expression = expression;
            _from = from;
            _to = to;
        }

        public static PlotData Deserialize(byte[] input)
        {
            using (var stream = new MemoryStream(input))
            {
                var formatter = new BinaryFormatter();
                stream.Seek(0, SeekOrigin.Begin);
                return (PlotData)formatter.Deserialize(stream);
            }
        }

        public static byte[] Serialize(PlotData input)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, input);
                stream.Flush();
                stream.Position = 0;
                return stream.ToArray();
            }
        }
    }
}
