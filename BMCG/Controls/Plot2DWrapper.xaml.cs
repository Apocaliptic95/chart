﻿using IronPlot;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BMCG
{
    public partial class Plot2DWrapper : UserControl, INotifyPropertyChanged
    {
        public Plot2DWrapper()
        {
            InitializeComponent();
            PropertyChanged += Plot2DWrapper_PropertyChanged;
        }

        public PlotData PlotData
        {
            get
            {
                return (PlotData)GetValue(PlotDataProperty);
            }
            set
            {
                SetValue(PlotDataProperty, value);
            }
        }

        private void DrawPlot()
        {
            this.Plot.Children.Clear();
            this.Plot.AddLine(PlotData.x, PlotData.y).Title = PlotData.Expression;
            this.Plot.Axes.YAxes.Left.FormatOverride = FormatOverrides.Currency;
        }

        public static readonly DependencyProperty PlotDataProperty = DependencyProperty.Register("PlotData", typeof(PlotData), typeof(Plot2DWrapper), new PropertyMetadata(default(PlotData), new PropertyChangedCallback(OnPlotDataChanged)));

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void Plot2DWrapper_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            DrawPlot();
        }

        private static void OnPlotDataChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Plot2DWrapper c = sender as Plot2DWrapper;
            if (c != null)
            {
                c.OnPropertyChanged("PlotData");
            }
        }
    }
}
